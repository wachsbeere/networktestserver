package com.hackapell;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public abstract class TCPServer implements Runnable {           // TCP Server，接收多个Client

    private int port;
    private boolean runFlag;            // 运行标记
    private List<SocketTransceiver> clients = new ArrayList<>();

    public TCPServer(int port) {
        this.port = port;
    }

    public void start() {
        runFlag = true;
        new Thread(this).start();
    }

    @Override
    public void run() {
        try {
            final ServerSocket server = new ServerSocket(port);
            while (runFlag) {
                try {
                    final Socket socket = server.accept();
                    startClient(socket);
                } catch (IOException e) {
                    e.printStackTrace();            // 接受客户端连接出错
                    this.onConnectFailed();
                }
            }
            try {
                for (SocketTransceiver client : clients) {
                    client.stop();
                }
                clients.clear();
                server.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.onServerStop();
    }

    private void startClient(final Socket socket) {
        SocketTransceiver client = new SocketTransceiver(socket) {

            @Override
            public void onReceive(InetAddress addr, String s) {
                TCPServer.this.onReceive(this, s);
            }

            @Override
            public void onDisconnect(InetAddress addr) {
//                stop();
                runFlag = false;
                clients.remove(this);
                TCPServer.this.onDisconnect(this);
            }
        };
        client.start();
        clients.add(client);
        this.onConnect(client);
    }

    public void stop() {
        runFlag = false;
    }

    public abstract void onConnect(SocketTransceiver client);

    public abstract void onConnectFailed();

    public abstract void onReceive(SocketTransceiver client, String s);

    public abstract void onDisconnect(SocketTransceiver client);

    public abstract void onServerStop();
}

