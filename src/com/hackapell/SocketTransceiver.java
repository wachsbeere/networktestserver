package com.hackapell;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

public abstract class SocketTransceiver implements Runnable {       // 管理单独Socket，控制输入和输出

    protected Socket socket;
    protected InetAddress addr;
    protected DataInputStream in;
    protected DataOutputStream out;
    private boolean runFlag;

    public SocketTransceiver(Socket socket) {
        this.socket = socket;
        this.addr = socket.getInetAddress();
    }

    public InetAddress getInetAddress() {
        return addr;
    }

    public void start() {
        runFlag = true;
        new Thread(this).start();
    }

    // 断开连接
    public void stop() {
        runFlag = false;
        try {
            socket.shutdownInput();
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 发送消息
    public boolean send(String s) {
        if (out != null) {
            try {
                out.writeUTF(s);
                out.flush();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public void run() {
        try {
            in = new DataInputStream(this.socket.getInputStream());
            out = new DataOutputStream(this.socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
            runFlag = false;
        }
        while (runFlag) {
            try {
                final String s = in.readUTF();
                this.onReceive(addr, s);
//                byte[] buffer = new byte[8192];
//                int readSize = in.read(buffer);
//                if (readSize == 0) continue;
//                if (readSize > 0) {
//                    String content = new String(buffer, 0, readSize);
//                    this.onReceive(addr, content);
//                }

                if (s.equals("disconnected")) {         // Client传来的终止信息
//                    this.onDisconnect(addr);
                    stop();
                    this.onDisconnect(addr);
                }

            } catch (IOException e) {
                runFlag = false;
            }
        }
        // 断开连接
        try {
            System.out.println("SocketTransceiver disconnected");
            in.close();
            out.close();
            socket.close();
            in = null;
            out = null;
            socket = null;
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.onDisconnect(addr);
    }

    public abstract void onReceive(InetAddress addr, String s);

    public abstract void onDisconnect(InetAddress addr);
}
